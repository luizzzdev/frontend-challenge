import React, { Suspense } from 'react';
import Navbar from './shared/Navbar/Navbar';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import routes from './shared/router';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Navbar />

        <Suspense fallback={<h1>Loading</h1>}>
          <Switch>
            {routes.map(route => {
              return (
                <Route
                  key={route.path}
                  path={route.path}
                  component={route.screen}
                ></Route>
              );
            })}
          </Switch>
        </Suspense>
      </BrowserRouter>
    </div>
  );
}

export default App;
