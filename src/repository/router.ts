import Repositories from './Repositories';
import PullRequests from './PullRequests';

const repositoryRoutes = [
  {
    path: '/repositories',
    screen: Repositories,
    label: 'Repositories',
    isExact: true,
    showOnNavBar: true,
  },
  {
    path: '/pull-requests/:owner/:name/pulls',
    screen: PullRequests,
    label: 'Pull Requests',
    isExact: true,
    showOnNavBar: false,
  },
];

export default repositoryRoutes;
