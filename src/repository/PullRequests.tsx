import React, { useEffect, useCallback, useState } from 'react';
import { useParams } from 'react-router-dom';
import Layout from '../shared/Layout/Layout';
import { RepositoryResource } from './RepositoryResource';
import { renderPullRequestsCards } from './helper';
import { PullRequest } from './PullRequest';
import Container from '../shared/Container/Container';

interface PullRequestsRouteParams {
  owner: string;
  name: string;
}

const PullRequests = () => {
  const params = useParams<PullRequestsRouteParams>();
  const [pulls, setPulls] = useState<Array<PullRequest>>([]);
  const [loading, setLoading] = useState(false);

  const fetchPullRequests = useCallback(async () => {
    try {
      setLoading(true);
      const data = await RepositoryResource.getRepositoryPullRequests(
        params.owner,
        params.name
      );

      setPulls(data);
    } catch (e) {
      console.error(e);
    } finally {
      setLoading(false);
    }
  }, [params.name, params.owner]);

  useEffect(() => {
    fetchPullRequests();
  }, [fetchPullRequests]);

  const shouldShowNoPullMessage = pulls.length === 0 && !loading;

  return (
    <Layout>
      <Container>{renderPullRequestsCards(pulls)}</Container>
      <Container>
        {shouldShowNoPullMessage && (
          <div className="content">
            <p>No pull requests ):</p>
          </div>
        )}
      </Container>
    </Layout>
  );
};

export default PullRequests;
