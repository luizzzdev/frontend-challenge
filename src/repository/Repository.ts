import { Owner } from '../shared/interfaces/Owner';

export interface Repository {
  id: number;
  name: string;
  description: string;
  owner: Owner;
  watchers_count: number;
  forks_count: number;
  html_url: string;
  pulls_url: string;
  node_id: string;
}
