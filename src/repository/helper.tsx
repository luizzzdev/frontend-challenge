import { Repository } from './Repository';
import React from 'react';
import FooterItem from '../shared/Card/FooterItem';
import Icon from '../shared/Icon/Icon';
import Card from '../shared/Card/Card';
import { css } from 'emotion';
import { PullRequest } from './PullRequest';
import { formatDate } from '../shared/utils/date';
import { truncate } from 'lodash';

const renderRepositoriesCards = (
  repositories: Array<Repository>,
  history: any
) =>
  repositories.map(repository => {
    const navigateHandler = () =>
      history.push(
        `/pull-requests/${repository.owner.login}/${repository.name}/pulls`
      );

    const footer = (
      <>
        <FooterItem>
          <a
            href={repository.html_url}
            target="_blank"
            rel="noopener noreferrer"
          >
            <Icon origin="fab" icon="github" />
          </a>
        </FooterItem>
        <FooterItem>
          <span
            className={
              'has-text-link has-text-weight-medium ' +
              css`
                cursor: pointer;
              `
            }
            onClick={navigateHandler}
          >
            Pulls
          </span>
        </FooterItem>
      </>
    );

    const subtitle = (
      <span className="is-size-7 has-text-grey">
        <span>
          <Icon icon="star" />
          <span>{repository.watchers_count}</span>
        </span>
        <span>
          <Icon icon="code-branch" />
          <span>{repository.forks_count}</span>
        </span>
      </span>
    );

    return (
      <div
        className={
          'column is-full-mobile is-one-third-desktop is-half-tablet is-one-quarter-widescreen'
        }
        key={repository.id}
      >
        <Card
          className={css`
            display: flex;
            height: 100%;
            justify-content: space-between;
            flex-direction: column;
          `}
          title={repository.name}
          subtitle={subtitle}
          content={truncate(repository.description, {
            length: 200,
            omission: '...',
          })}
          image={repository.owner.avatar_url}
          imageAlt={repository.owner.login}
          footer={footer}
        />
      </div>
    );
  });

const renderPullRequestsCards = (pullRequests: Array<PullRequest>) =>
  pullRequests.map(pull => {
    const footer = (
      <FooterItem>
        <a href={pull.html_url} target="_blank" rel="noopener noreferrer">
          <Icon origin="fab" icon="github" />
        </a>
      </FooterItem>
    );

    return (
      <div
        className={
          'column is-full-mobile is-one-half-desktop is-half-tablet is-one-half-widescreen'
        }
        key={pull.id}
      >
        <Card
          className={css`
            display: flex;
            height: 100%;
            justify-content: space-between;
            flex-direction: column;
          `}
          title={pull.title}
          subtitle={formatDate(pull.created_at)}
          content={<div dangerouslySetInnerHTML={{ __html: pull.body }}></div>}
          image={pull.user.avatar_url}
          imageAlt={pull.user.login}
          footer={footer}
        />
      </div>
    );
  });

export { renderRepositoriesCards, renderPullRequestsCards };
