import { Owner } from '../shared/interfaces/Owner';

export interface PullRequest {
  id: number;
  html_url: string;
  user: Owner;
  body: string;
  title: string;
  created_at: string;
}
