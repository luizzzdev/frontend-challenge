import { Api } from '../shared/api/base';
import { Repository } from './Repository';
import { GithubResponse } from '../shared/api/GithubResponse';

interface getJavascriptRepositoriesParams {
  q?: string;
  page: number;
  sort?: string;
}

const path = '/search/repositories';

export const RepositoryResource = {
  async getJavascriptRepositories({
    page,
    q,
    sort = 'stars',
  }: getJavascriptRepositoriesParams): Promise<GithubResponse<Repository>> {
    const query: string[] = ['language: Javascript'];

    if (q) {
      query.push(q);
    }

    const params = {
      page,
      sort,
      q: query.join(' '),
    };

    const response = await Api.get(path, { params });

    return response.data;
  },

  async getRepositoryPullRequests(owner: string, repositoryName: string) {
    const path = `/repos/${owner}/${repositoryName}/pulls`;

    const response = await Api.get(path);

    return response.data;
  },
};
