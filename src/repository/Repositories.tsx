import React, { useEffect, useCallback, useState, useMemo } from 'react';
import { RepositoryResource } from './RepositoryResource';
import { Repository } from './Repository';
import { css } from 'emotion';
import Layout from '../shared/Layout/Layout';
import { useDebounce } from 'use-debounce';
import { renderRepositoriesCards } from './helper';
import { useHistory } from 'react-router-dom';
import Select from '../shared/Select/Select';
import Container from '../shared/Container/Container';
import Input from '../shared/Input/Input';
import InfiniteScroll from '../shared/InfiniteScroll/InfiniteScroll';

interface Repositories {
  history: History;
}

const sortOptions = [
  {
    value: 'stars',
    label: 'Stars',
  },
  {
    value: 'forks',
    label: 'Forks',
  },
];

const Repositories: React.FC<Repositories> = () => {
  const [repositories, setRepositories] = useState<Array<Repository>>([]);
  const [loading, setLoading] = useState(false);
  const [q, setQ] = useState('');
  const [debouncedQ] = useDebounce(q, 300);
  const [sortBy, setSortBy] = useState('stars');
  const [page, setPage] = useState(1);

  const history = useHistory();

  const params = useMemo(
    () => ({
      page,
      q: debouncedQ,
      sort: sortBy,
    }),
    [debouncedQ, page, sortBy]
  );

  const accumulateRepositories = useCallback(async () => {
    try {
      setLoading(true);
      const result = await RepositoryResource.getJavascriptRepositories({
        ...params,
        page,
      });
      setRepositories(prevState => [...prevState, ...result.items]);
    } catch (e) {
      console.error(e);
    } finally {
      setLoading(false);
    }
  }, [page, params]);

  useEffect(() => {
    accumulateRepositories();
  }, [accumulateRepositories]);

  const loadMore = useCallback(() => {
    setLoading(true);
    setPage(previous => previous + 1);
  }, []);

  const onChangeInputHandler = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setRepositories([]);
      setQ(event.target.value);
    },
    []
  );

  const onChangeSortByHandler = useCallback(
    (event: React.ChangeEvent<HTMLSelectElement>) => {
      setRepositories([]);
      setSortBy(event.target.value);
    },
    []
  );

  return (
    <Layout>
      <InfiniteScroll loadMore={loadMore} loading={loading}>
        <div className="control columns">
          <div className="field column">
            <Input
              placeholder="Search for repositories"
              value={q}
              onChange={onChangeInputHandler}
              loading={loading}
            />
          </div>

          <div className="field column">
            <Select
              options={sortOptions}
              value={sortBy}
              onChange={onChangeSortByHandler}
              loading={loading}
            />
          </div>
        </div>
        <Container
          className={css`
            justify-content: center;
          `}
        >
          {renderRepositoriesCards(repositories, history)}
        </Container>
      </InfiniteScroll>
    </Layout>
  );
};

export default Repositories;
