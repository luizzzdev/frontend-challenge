import React from 'react';
import classes from './Icon.module.scss';

interface Icon {
  icon: string;
  origin?: string;
}

const Icon: React.FC<Icon> = ({ icon, origin = 'fa' }) => {
  return <i className={`${origin} fa-${icon} ` + classes.icon}></i>;
};

export default Icon;
