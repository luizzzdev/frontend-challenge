import React from 'react';
import routes from '../router';
import { NavLink } from 'react-router-dom';

const Navbar = () => {
  return (
    <nav
      className={'navbar is-info'}
      role="navigation"
      aria-label="main navigation"
    >
      <div id="navbarBasicExample" className="navbar-menu">
        <div className="navbar-start">
          {routes.map(route => {
            return (
              route.showOnNavBar && (
                <NavLink
                  className="navbar-item"
                  to={route.path}
                  exact={route.isExact}
                  key={route.path}
                >
                  {route.label}
                </NavLink>
              )
            );
          })}
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
