import React from 'react';

interface FooterItem {
  children: React.ReactNode;
}

const FooterItem: React.FC<FooterItem> = ({ children }) => {
  return <div className="card-footer-item">{children}</div>;
};

export default FooterItem;
