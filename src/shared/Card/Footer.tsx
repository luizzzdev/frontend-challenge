import React from 'react';

interface Footer {
  children: React.ReactNode;
}

const Footer: React.FC<Footer> = ({ children }) => {
  return (
    <footer className="card-footer">
      {children}
    </footer>
  );
};

export default Footer;


