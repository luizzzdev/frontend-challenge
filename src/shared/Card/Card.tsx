import React from 'react';

interface Card {
  image?: string;
  imageAlt?: string;
  title?: string;
  subtitle?: string | React.ReactNode;
  content?: string | React.ReactNode;
  className?: string;
  footer: React.ReactNode;
}

const Card: React.FC<Card> = ({
  image,
  imageAlt,
  title,
  subtitle,
  content,
  className,
  footer,
}) => {
  return (
    <div className={(className || '') + ' card'}>
      <div className="card-content">
        <div className="media">
          <div className="media-left">
            <figure className="image is-48x48">
              <img src={image} alt={imageAlt} />
            </figure>
          </div>
          <div className="media-content">
            <p className="title is-4">{title}</p>
            <p className="subtitle is-6">{subtitle}</p>
          </div>
        </div>

        <div className="content">{content}</div>
      </div>
      <footer className="card-footer">{footer}</footer>
    </div>
  );
};

export default Card;
