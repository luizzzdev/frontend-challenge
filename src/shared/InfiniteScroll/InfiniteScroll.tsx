import React, { useRef, useEffect } from 'react';
import { debounce } from 'lodash';

interface InfiniteScrollProps {
  loadMore: Function;
  className?: string;
  children: React.ReactNode;
  loading: boolean;
}

const InfiniteScroll: React.FC<InfiniteScrollProps> = ({
  loadMore,
  children,
  className,
  loading,
}) => {
  const scroll = useRef<any>();

  const debouncedLoadMore = debounce(() => {
    loadMore();
  }, 150);

  useEffect(() => {
    const listener = () => {
      const screenHeight = window.innerHeight;
      const heightToScroll =
        document.body.getBoundingClientRect().height - screenHeight * 2;
      const scrollPosition = window.pageYOffset;
      const scrolledEnough = scrollPosition > heightToScroll;

      if (scrolledEnough && !loading) {
        debouncedLoadMore();
      }
    };

    window.addEventListener('scroll', listener);

    return () => window.removeEventListener('scroll', listener);
  }, [debouncedLoadMore, loadMore, loading]);

  return (
    <div className={className} ref={scroll}>
      {children}
    </div>
  );
};

export default InfiniteScroll;
