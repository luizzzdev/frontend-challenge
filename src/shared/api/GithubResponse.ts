export interface GithubResponse<T> {
  total_count: number;
  incomplete_results: false;
  items: Array<T>;
}
