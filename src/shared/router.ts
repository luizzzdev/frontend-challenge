import repositoryRoutes from '../repository/router';

const routes = [...repositoryRoutes];

export default routes;
