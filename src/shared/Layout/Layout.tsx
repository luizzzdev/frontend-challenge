import { css } from 'emotion';
import React from 'react';

interface Layout {
  children: React.ReactNode;
}

const Layout: React.FC<Layout> = ({ children }) => {
  return (
    <div
      className={css`
        padding: 10px;
      `}
    >
      {children}
    </div>
  );
};

export default Layout;
