import React, { ChangeEventHandler } from 'react';
import classnames from 'classnames';

interface Selectable {
  value: string;
  label: string;
}

interface SelectProps {
  options: Array<Selectable>;
  value: string;
  onChange: ChangeEventHandler<HTMLSelectElement>;
  loading?: boolean;
}

const Select: React.FC<SelectProps> = ({
  options,
  value,
  onChange,
  loading,
}) => {
  return (
    <div className={classnames('select', 'field', { 'is-loading': loading })}>
      <select onChange={onChange} value={value}>
        {options.map(option => {
          return (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          );
        })}
      </select>
    </div>
  );
};

export default Select;
