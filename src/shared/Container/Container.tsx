import React from 'react';

interface Container {
  children: React.ReactNode;
  className?: string;
}

const Container: React.FC<Container> = ({ children, className }) => {
  return (
    <div className={'container columns is-multiline is-fluid ' + className}>
      {children}
    </div>
  );
};

export default Container;
