import React, { ChangeEventHandler } from 'react';
import classnames from 'classnames';

interface InputProps {
  value: string;
  onChange: ChangeEventHandler<HTMLInputElement>;
  loading?: boolean;
  type?: string;
  placeholder: string;
}

const Input: React.FC<InputProps> = ({
  value,
  loading,
  onChange,
  type = 'text',
  placeholder,
}) => {
  return (
    <div className={classnames('control', { 'is-loading': loading })}>
      <input
        className={classnames('input')}
        type={type}
        placeholder={placeholder}
        onChange={onChange}
        value={value}
      />
    </div>
  );
};

export default Input;
